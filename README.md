# mondia-media-task
This application using Spring boot Technology for more info. check this url: https://projects.spring.io/spring-boot/


## Installation Steps and Required tools and technologies.
 1- Java 1.8, To install Java 1.8 check this url: https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html
    for checking if it installed, please run "java -version" command on your terminal, to get the version of installed java on you machine.
 
 2- Maven, To install Maven check installation steps here: https://maven.apache.org/index.html
    for checking if it installed, please run "mvn -v" command on your terminal, to get the version of installed maven on you machine.
 
 3- Mysql 5.7, To install Mysql check this url: https://dev.mysql.com/doc/refman/5.7/en/installing.html
    for checking if it installed, please run "mysql -v" command on your terminal, to get the version of installed Mysql on you machine.
    


## Run steps
1- Create mysql database support utf8 by running this command 
        
        "CREATE DATABASE <your-database-name> CHARACTER SET utf8 COLLATE utf8_general_ci;" on mysql terminal, Don't forget to change the database name as you prefer.

2- Change database configuration in application.properties in src/main/resources/
    you just need to add change 
    
        spring.datasource.url value with <your-database-name>, 
        spring.datasource.username with <your-database-user>
        spring.datasource.password with <your-database-password>

3- running maven command for getting dependencies, installing application, create database tables and running test cases.

         mvn install
         
## APIs Documentation

        Please Check the APIs Documentation api_documentation.docx and mondia_media.postman_collection.json