package com.mondia.task.repository;

import com.mondia.task.domain.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@SuppressWarnings("unused")
@Repository
public interface ServiceRepository extends JpaRepository<ServiceEntity, Long> {

}
