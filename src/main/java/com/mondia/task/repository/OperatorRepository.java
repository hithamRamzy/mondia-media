package com.mondia.task.repository;

import com.mondia.task.domain.OperatorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@SuppressWarnings("unused")
@Repository
public interface OperatorRepository extends JpaRepository<OperatorEntity, Long> {

}
