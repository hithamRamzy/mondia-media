package com.mondia.task.service;

import com.mondia.task.domain.ServiceEntity;
import com.mondia.task.repository.ServiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceService {

    private final ServiceRepository serviceRepository;

    public ServiceService(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    public ServiceEntity save(ServiceEntity serviceEntity) {
        return serviceRepository.save(serviceEntity);
    }

    public List<ServiceEntity> findAll() {
        return serviceRepository.findAll();
    }

    public ServiceEntity findOne(Long id) {
        return serviceRepository.findOne(id);
    }

    public void delete(Long id) {
        serviceRepository.delete(id);
    }
}