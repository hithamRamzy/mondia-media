package com.mondia.task.service;

import com.mondia.task.domain.OperatorEntity;
import com.mondia.task.repository.OperatorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperatorService {

    OperatorRepository operatorRepository;

    public OperatorService(OperatorRepository operatorRepository) {
        this.operatorRepository = operatorRepository;
    }

    public OperatorEntity save(OperatorEntity operatorEntity) {
        return operatorRepository.save(operatorEntity);
    }

    public List<OperatorEntity> findAll() {
        return operatorRepository.findAll();
    }

    public OperatorEntity findOne(Long id) {
        return operatorRepository.findOne(id);
    }

    public void delete(Long id) {
        operatorRepository.delete(id);
    }
}