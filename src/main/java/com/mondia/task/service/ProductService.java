package com.mondia.task.service;

import com.mondia.task.domain.ProductEntity;
import com.mondia.task.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public ProductEntity save(ProductEntity productEntity) {
        return productRepository.save(productEntity);
    }

    public List<ProductEntity> findAll() {
        return productRepository.findAll();
    }

    public ProductEntity findOne(Long id) {
        return productRepository.findOne(id);
    }

    public void delete(Long id) {
        productRepository.delete(id);
    }
}