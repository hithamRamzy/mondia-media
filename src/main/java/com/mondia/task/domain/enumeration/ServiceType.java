package com.mondia.task.domain.enumeration;

public enum ServiceType {

    SUBSCRIPTION(1, "Subscription"), ALACARTE(2, "Alacarte");

    private Integer id;
    private String name;

    ServiceType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ServiceType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
