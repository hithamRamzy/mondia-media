package com.mondia.task.domain;


import com.mondia.task.domain.enumeration.ServiceType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "service")
public class ServiceEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "service_type", nullable = false)
    private ServiceType type;

    @Column(name = "operator_service_id")
    private Integer operatorServiceId;

    @Column(name = "operator_package_id")
    private Integer operatorPackageId;

    @ManyToOne(cascade = CascadeType.ALL)
    private ProductEntity productEntity;

    @ManyToOne(cascade = CascadeType.ALL)
    private OperatorEntity operatorEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ServiceEntity name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServiceType getType() {
        return type;
    }

    public ServiceEntity type(ServiceType type) {
        this.type = type;
        return this;
    }

    public void setType(ServiceType type) {
        this.type = type;
    }

    public Integer getOperatorServiceId() {
        return operatorServiceId;
    }

    public void setOperatorServiceId(Integer operatorServiceId) {
        this.operatorServiceId = operatorServiceId;
    }

    public Integer getOperatorPackageId() {
        return operatorPackageId;
    }

    public ServiceEntity operatorPackageId(Integer operatorPackageId) {
        this.operatorPackageId = operatorPackageId;
        return this;
    }

    public void setOperatorPackageId(Integer operatorPackageId) {
        this.operatorPackageId = operatorPackageId;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public ServiceEntity product(ProductEntity productEntity) {
        this.productEntity = productEntity;
        return this;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public OperatorEntity getOperatorEntity() {
        return operatorEntity;
    }

    public ServiceEntity operators(OperatorEntity operatorEntity) {
        this.operatorEntity = operatorEntity;
        return this;
    }

    public void setOperatorEntity(OperatorEntity operatorEntity) {
        this.operatorEntity = operatorEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceEntity serviceEntity = (ServiceEntity) o;
        if (serviceEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), serviceEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ServiceEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", operatorServiceId=" + operatorServiceId +
                ", operatorPackageId=" + operatorPackageId +
                ", productEntity=" + productEntity +
                ", operatorEntity=" + operatorEntity +
                '}';
    }
}
