package com.mondia.task;

import com.mondia.task.domain.Role;
import com.mondia.task.domain.User;
import com.mondia.task.domain.dto.UserDTO;
import com.mondia.task.repository.UserRepository;
import com.mondia.task.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

@SpringBootApplication
public class MondiaApplication {

    @Autowired
    private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(MondiaApplication.class, args);
	}

    @Autowired
    public void authenticationManager(AuthenticationManagerBuilder builder, UserRepository repository, UserService userService) throws Exception {
        if (repository.count()==0)
            userService.save(new User("user", "password", Arrays.asList(new Role("USER"))));
        builder.userDetailsService(userDetailsService(repository)).passwordEncoder(passwordEncoder);
    }


    private UserDetailsService userDetailsService(final UserRepository repository) {
        return username -> new UserDTO(repository.findByUsername(username));
    }
}
