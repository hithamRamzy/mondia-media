package com.mondia.task.rest;

import com.mondia.task.domain.ServiceEntity;
import com.mondia.task.rest.util.HeaderUtil;
import com.mondia.task.rest.util.ResponseUtil;
import com.mondia.task.service.ServiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ServiceEntity.
 */
@RestController
@RequestMapping("/api")
public class ServiceResource {

    private final Logger log = LoggerFactory.getLogger(ServiceResource.class);

    private static final String ENTITY_NAME = "service";

    private final ServiceService serviceService;

    public ServiceResource(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @PostMapping("/services")
    public ResponseEntity<ServiceEntity> createService(@Valid @RequestBody ServiceEntity serviceEntity) throws Exception {
        log.debug("REST request to save ServiceEntity : {}", serviceEntity);
        if (serviceEntity.getId() != null) {
            throw new Exception("A new serviceEntity cannot already have an ID");
        }
        ServiceEntity result = serviceService.save(serviceEntity);
        return ResponseEntity.created(new URI("/api/services/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @PutMapping("/services")
    public ResponseEntity<ServiceEntity> updateService(@Valid @RequestBody ServiceEntity serviceEntity) throws Exception {
        log.debug("REST request to update ServiceEntity : {}", serviceEntity);
        if (serviceEntity.getId() == null) {
            return createService(serviceEntity);
        }
        ServiceEntity result = serviceService.save(serviceEntity);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceEntity.getId().toString()))
                .body(result);
    }

    @GetMapping("/services")
    public List<ServiceEntity> getAllServices() {
        log.debug("REST request to get all Services");
        return serviceService.findAll();
    }

    @GetMapping("/services/{id}")
    public ResponseEntity<ServiceEntity> getService(@PathVariable Long id) {
        log.debug("REST request to get ServiceEntity : {}", id);
        ServiceEntity serviceEntity = serviceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceEntity));
    }

    @DeleteMapping("/services/{id}")
    public ResponseEntity<Void> deleteService(@PathVariable Long id) {
        log.debug("REST request to delete ServiceEntity : {}", id);
        serviceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
