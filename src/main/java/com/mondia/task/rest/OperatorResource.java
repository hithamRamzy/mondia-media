package com.mondia.task.rest;

import com.mondia.task.domain.OperatorEntity;
import com.mondia.task.rest.util.HeaderUtil;
import com.mondia.task.rest.util.ResponseUtil;
import com.mondia.task.service.OperatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class OperatorResource {

    private final Logger log = LoggerFactory.getLogger(OperatorResource.class);

    private static final String ENTITY_NAME = "operator";

    private final OperatorService operatorService;

    public OperatorResource(OperatorService operatorService) {
        this.operatorService = operatorService;
    }


    @PostMapping("/operators")
    public ResponseEntity<OperatorEntity> createOperator(@Valid @RequestBody OperatorEntity operatorEntity) throws Exception {
        log.debug("REST request to save OperatorEntity : {}", operatorEntity);
        if (operatorEntity.getId() != null) {
            throw new Exception("A new operatorEntity cannot already have an ID");
        }
        OperatorEntity result = operatorService.save(operatorEntity);
        return ResponseEntity.created(new URI("/api/operators/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @PutMapping("/operators")
    public ResponseEntity<OperatorEntity> updateOperator(@Valid @RequestBody OperatorEntity operatorEntity) throws Exception {
        log.debug("REST request to update OperatorEntity : {}", operatorEntity);
        if (operatorEntity.getId() == null) {
            return createOperator(operatorEntity);
        }
        OperatorEntity result = operatorService.save(operatorEntity);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, operatorEntity.getId().toString()))
                .body(result);
    }


    @GetMapping("/operators")
    public List<OperatorEntity> getAllOperators() {
        log.debug("REST request to get all Operators");
        return operatorService.findAll();
    }


    @GetMapping("/operators/{id}")
    public ResponseEntity<OperatorEntity> getOperator(@PathVariable Long id) {
        log.debug("REST request to get OperatorEntity : {}", id);
        OperatorEntity operatorEntity = operatorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(operatorEntity));
    }


    @DeleteMapping("/operators/{id}")
    public ResponseEntity<Void> deleteOperator(@PathVariable Long id) {
        log.debug("REST request to delete OperatorEntity : {}", id);
        operatorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
