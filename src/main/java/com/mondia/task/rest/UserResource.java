package com.mondia.task.rest;


import com.mondia.task.domain.Role;
import com.mondia.task.domain.User;
import com.mondia.task.domain.dto.UserDTO;
import com.mondia.task.repository.UserRepository;
import com.mondia.task.rest.util.HeaderUtil;
import com.mondia.task.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.Arrays;

@RestController
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    public UserResource(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }


    @PostMapping("/register")
    public ResponseEntity<User> register(@Valid @RequestBody UserDTO userDTO) throws Exception {
        log.debug("REST request to register User : {}", userDTO);

        if (userRepository.findByUsername(userDTO.getUsername().toLowerCase()) != null) {
            throw new Exception();
        } else {
            User newUser = userService.save(new User(userDTO.getUsername(), userDTO.getPassword(), Arrays.asList(new Role("USER"), new Role("ACTUATOR"))));
            return ResponseEntity.created(new URI("/api/users/" + newUser.getUsername()))
                    .headers(HeaderUtil.createAlert("A user is created with identifier " + newUser.getUsername(), newUser.getUsername()))
                    .body(newUser);
        }
    }

}
