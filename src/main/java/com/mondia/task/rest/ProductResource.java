package com.mondia.task.rest;

import com.mondia.task.domain.ProductEntity;
import com.mondia.task.rest.util.HeaderUtil;
import com.mondia.task.rest.util.ResponseUtil;
import com.mondia.task.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ProductResource {

    private final Logger log = LoggerFactory.getLogger(ProductResource.class);

    private static final String ENTITY_NAME = "product";

    private final ProductService productService;

    public ProductResource(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/products")
    public ResponseEntity<ProductEntity> createProduct(@Valid @RequestBody ProductEntity productEntity) throws Exception {
        log.debug("REST request to save ProductEntity : {}", productEntity);
        if (productEntity.getId() != null) {
            throw new Exception("A new productEntity cannot already have an ID");
        }
        ProductEntity result = productService.save(productEntity);
        return ResponseEntity.created(new URI("/api/products/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @PutMapping("/products")
    public ResponseEntity<ProductEntity> updateProduct(@Valid @RequestBody ProductEntity productEntity) throws Exception {
        log.debug("REST request to update ProductEntity : {}", productEntity);
        if (productEntity.getId() == null) {
            return createProduct(productEntity);
        }
        ProductEntity result = productService.save(productEntity);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productEntity.getId().toString()))
                .body(result);
    }

    @GetMapping("/products")
    public List<ProductEntity> getAllProducts() {
        log.debug("REST request to get all Products");
        return productService.findAll();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<ProductEntity> getProduct(@PathVariable Long id) {
        log.debug("REST request to get ProductEntity : {}", id);
        ProductEntity productEntity = productService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productEntity));
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        log.debug("REST request to delete ProductEntity : {}", id);
        productService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
