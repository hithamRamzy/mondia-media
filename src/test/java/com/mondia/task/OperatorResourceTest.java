package com.mondia.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mondia.task.domain.OperatorEntity;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

//@Transactional  could be used if you need this test class running without affecting DB
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //This Annotation for avoiding unpredictable test cases sorting
public class OperatorResourceTest {

    @Autowired
    private MockMvc mvc;

    private static String access_token = "";
    private static OperatorEntity operatorEntity;
    static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeClass
    public static void createOperatorObject() {
        operatorEntity = new OperatorEntity();
        operatorEntity.setName("Vodafone");
        operatorEntity.setCountry("Egypt");
    }

    @Test
    public void login() throws Exception {
        String content = mvc.perform(MockMvcRequestBuilders.post("/oauth/token?grant_type=password&username=user&password=password")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).header("Authorization","Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        access_token = content.substring(17, 53);
    }

    @Test
    public void testAAddOperator() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/api/operators")
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(operatorEntity)))
                .andExpect(status().isCreated()).andReturn();
        updateStaticOperator(result);
    }

    @Test
    public void testBGetOperator() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/api/operators/" + operatorEntity.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(operatorEntity)))
                .andExpect(status().isOk()).andReturn();
        updateStaticOperator(result);
    }

    @Test
    public void testCUpdateOperator() throws Exception {
        operatorEntity.setCountry("UAE");
        operatorEntity.setName("Orange");
        MvcResult result = mvc.perform(MockMvcRequestBuilders.put("/api/operators")
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(operatorEntity)))
                .andExpect(status().isOk()).andReturn();
        updateStaticOperator(result);
    }

    @Test
    public void testDDeleteOperator() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/api/operators/" + operatorEntity.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(operatorEntity)))
                .andExpect(status().isOk()).andReturn();
    }

    private static String convertIntoJson(OperatorEntity operatorEntity) throws JsonProcessingException {
        return objectMapper.writeValueAsString(operatorEntity);
    }

    private static OperatorEntity convertFromJson(String jsonString) throws IOException {
        return objectMapper.readValue(jsonString, OperatorEntity.class);
    }

    private static void updateStaticOperator(MvcResult mvcResult) throws IOException {
        operatorEntity = convertFromJson(mvcResult.getResponse().getContentAsString());
    }
}