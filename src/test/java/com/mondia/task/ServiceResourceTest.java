package com.mondia.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mondia.task.domain.OperatorEntity;
import com.mondia.task.domain.ProductEntity;
import com.mondia.task.domain.ServiceEntity;
import com.mondia.task.domain.enumeration.ServiceType;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

//@Transactional  could be used if you need this test class running without affecting DB
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //This Annotation for avoiding unpredictable test cases sorting
public class ServiceResourceTest {

    @Autowired
    private MockMvc mvc;

    private static String access_token = "";
    private static ServiceEntity serviceEntity;
    static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeClass
    public static void createServiceObject() {
        serviceEntity = new ServiceEntity();
        serviceEntity.setName("ServiceEntity Name for Testing");
        serviceEntity.setType(ServiceType.SUBSCRIPTION);
        serviceEntity.setOperatorPackageId(1);
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName("ProductEntity Name for Testing");
        productEntity.setDescription("ProductEntity Description for Testing");
        productEntity.setMinPrice(10d);
        productEntity.setMaxPrice(20d);
        serviceEntity.setProductEntity(productEntity);

        OperatorEntity operatorEntity = new OperatorEntity();
        operatorEntity.setName("Vodafone");
        operatorEntity.setCountry("Egypt");
        serviceEntity.setOperatorEntity(operatorEntity);
    }

    @Test
    public void login() throws Exception {
        String content = mvc.perform(MockMvcRequestBuilders.post("/oauth/token?grant_type=password&username=user&password=password")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).header("Authorization","Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        access_token = content.substring(17, 53);
    }


    @Test
    public void testAAddService() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/api/services")
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(serviceEntity)))
                .andExpect(status().isCreated()).andReturn();
        updateStaticService(result);
    }

    @Test
    public void testBGetService() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/api/services/" + serviceEntity.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(serviceEntity)))
                .andExpect(status().isOk()).andReturn();
        updateStaticService(result);
    }

    @Test
    public void testCUpdateService() throws Exception {
        serviceEntity.setName("Other ServiceEntity Name");
        serviceEntity.setType(ServiceType.ALACARTE);
        MvcResult result = mvc.perform(MockMvcRequestBuilders.put("/api/services")
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(serviceEntity)))
                .andExpect(status().isOk()).andReturn();
        updateStaticService(result);
    }

    @Test
    public void testDDeleteService() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/api/services/" + serviceEntity.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(serviceEntity)))
                .andExpect(status().isOk()).andReturn();
    }

    private static String convertIntoJson(ServiceEntity serviceEntity) throws JsonProcessingException {
        return objectMapper.writeValueAsString(serviceEntity);
    }

    private static ServiceEntity convertFromJson(String jsonString) throws IOException {
        return objectMapper.readValue(jsonString, ServiceEntity.class);
    }

    private static void updateStaticService(MvcResult mvcResult) throws IOException {
        serviceEntity = convertFromJson(mvcResult.getResponse().getContentAsString());
    }
}