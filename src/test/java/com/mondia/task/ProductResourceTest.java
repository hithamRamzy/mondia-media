package com.mondia.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mondia.task.domain.ProductEntity;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

//@Transactional  could be used if you need this test class running without affecting DB
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //This Annotation for avoiding unpredictable test cases sorting
public class ProductResourceTest {

    @Autowired
    private MockMvc mvc;

    private static String access_token = "";
    private static ProductEntity productEntity;
    static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeClass
    public static void createProductObject() {
        productEntity = new ProductEntity();
        productEntity.setName("ProductEntity Name for Testing");
        productEntity.setDescription("ProductEntity Description for Testing");
        productEntity.setMinPrice(10d);
        productEntity.setMaxPrice(20d);
    }

    @Test
    public void login() throws Exception {
        String content = mvc.perform(MockMvcRequestBuilders.post("/oauth/token?grant_type=password&username=user&password=password")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).header("Authorization","Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        access_token = content.substring(17, 53);
    }

    @Test
    public void testAAddProduct() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(productEntity)))
                .andExpect(status().isCreated()).andReturn();
        updateStaticProduct(result);
    }

    @Test
    public void testBGetProduct() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/api/products/" + productEntity.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(productEntity)))
                .andExpect(status().isOk()).andReturn();
        updateStaticProduct(result);
    }

    @Test
    public void testCUpdateProduct() throws Exception {
        productEntity.setName("Other ProductEntity Name");
        productEntity.setDescription("Other ProductEntity Description");
        MvcResult result = mvc.perform(MockMvcRequestBuilders.put("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(productEntity)))
                .andExpect(status().isOk()).andReturn();
        updateStaticProduct(result);
    }

    @Test
    public void testDDeleteProduct() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/api/products/" + productEntity.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .param("access_token",access_token)
                .content(convertIntoJson(productEntity)))
                .andExpect(status().isOk()).andReturn();
    }

    private static String convertIntoJson(ProductEntity productEntity) throws JsonProcessingException {
        return objectMapper.writeValueAsString(productEntity);
    }

    private static ProductEntity convertFromJson(String jsonString) throws IOException {
        return objectMapper.readValue(jsonString, ProductEntity.class);
    }

    private static void updateStaticProduct(MvcResult mvcResult) throws IOException {
        productEntity = convertFromJson(mvcResult.getResponse().getContentAsString());
    }
}